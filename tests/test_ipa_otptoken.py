#!/usr/libexec/platform-python
"""Simple test for APIs used by python3-qrcode
"""
import io
import logging
import hashlib

import qrcode

logging.basicConfig(level=logging.INFO)
log = logging.getLogger()

TEXT = "example data"
HASH = "45100bc2be879090a4b219d4adf9e8342e123ba8cd74769343ef4b3656a3dc2f"


def main():
    qr_output = io.StringIO()
    qr = qrcode.QRCode()
    qr.add_data(TEXT)
    qr.make()
    qr.print_ascii(out=qr_output, tty=False)
    value = qr_output.getvalue()
    print(value)
    assert hashlib.sha256(value.encode('utf-8')).hexdigest() == HASH
    log.info("PASS")


if __name__ == "__main__":
    main()
